## GrandysoftTestApp
```
cd existing_repo
git remote add origin https://gitlab.com/tyiiikah4ik/GrandysoftTestApp.git
```
## Deployment and launch

```bash
# requires installation:
node.js (https://nodejs.org/en/)
docker (https://www.docker.com/)

# install necessary package
$ npm install --legacy-peer-deps

# initialize docker container
$ docker-compose up

# edit .env file for change:
#fake user count
SEED_USER_COUNT = 200

#maximum user following count
SEED_SUBSCRIBE_COUNT = 150

# seed db with fake data
$ npm run seed

# initialize app instance
$ npm run start
```

## app endpoints

```bash
# getting all users with subscribe, selected 10 per page
$ send GET request to localhost:3000/users/page/:pageNumber

# find user by id with all friends
$ send GET reques to localhost:3000/users/:userId/friends

# find users with no following 
$ send GET request to localhost:3000/users/no-following

# find users with the largest number of subscriptions
$ send GET request to localhost:3000/users/max-following

# create new user
$ send POST request to localhost:3000/users/create
with JSON Body: 
{
    "name": "userName",
    "gender": "userGender(male|female)"
}

# subscribe to user
$ send POST request to localhost:3000/users/:userId/subscribe
with JSON Body:
{    
    "toSubscribeId": "number"
}

```