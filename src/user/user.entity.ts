import 'reflect-metadata';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  Index,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { FollowingCount } from './followingCount.entity';
import { Gender } from './gender.enum';


@Entity()
export class User {

  @Index()
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'varchar',
    length: 200
  })
  name!: string;

  @Column({
    type: 'varchar',
    length: 10
  })
  gender!: Gender;

  @ManyToMany(() => User, (user) => user.following)
  @JoinTable()
  followers!: Promise<User[]>;

  @ManyToMany(() => User, (user) => user.followers)
  following!: Promise<User[]>;

  @OneToOne(() => FollowingCount, (followingCounter) => followingCounter.user)
  followingCount: Promise<FollowingCount>;

};