import 'reflect-metadata';
import { UserService } from "./user.service";
import { Body, Controller, Get, HttpError, Param, Post } from "routing-controllers";
import { UserDTO } from "./dto/user.dto";
import { subscribeDTO } from './dto/subscribe.dto';


@Controller('/users')
export class UserController {
  private readonly userService = new UserService();

  @Post('/create')
  async createUser(@Body() data: UserDTO) {
    return await this.userService.createUser(data);
  };

  @Get('/page/:pageNumber')
  async getAll(@Param('pageNumber') pageNumber: number) {
    return await this.userService.findAllUser(pageNumber);
  };

  @Post('/:id/subscribe')
  async subscribe(@Param('id') userId: number, @Body() data: subscribeDTO) {

    if (userId != data.toSubscribeId) {
      return await this.userService.userSubscribe(userId, data.toSubscribeId);
    };
    throw new HttpError(403, "You cant subscribe on yourself");
  };

  @Get('/:id/friends')
  async getFriends(@Param('id') userId: number) {
    return await this.userService.findUserFriends(userId);
  };

  @Get('/no-following')
  async noFollowing() {
    return await this.userService.findUsersWithNoFollowing();
  };

  @Get('/max-following')
  async maxFollowing() {
    return await this.userService.findTopFollowers();
  }

};
