import { isNotEmpty } from 'class-validator';
import 'reflect-metadata';
import { HttpError } from 'routing-controllers';
import { AppDataSource } from "../data-source";
import { UserDTO } from "./dto/User.dto";
import { FollowingCount } from './followingCount.entity';
import { User } from "./user.entity";


export class UserService {
  private readonly userRepository = AppDataSource.getRepository(User);
  private readonly followingCountRepository = AppDataSource.getRepository(FollowingCount);

  async createUser(data: UserDTO) {
    let user = new User();
    let followingCounter = new FollowingCount();

    user.name = data.name;
    user.gender = data.gender;
    user = await this.userRepository.save(user);

    followingCounter.user = user;
    followingCounter.followingCounter = 0;
    await this.followingCountRepository.save(followingCounter);

    return { user };

  };

  async findAllUser(pageNumber: number) {

    const take = 10;
    const page = pageNumber || 1;
    const skip = (page - 1) * take;

    const [result, total] = await this.userRepository.findAndCount({
      relations: {
        following: true,
        followingCount: true,
      },
      take: take,
      skip: skip
    });

    return { result, total };

  };

  async userSubscribe(subscriberId: number, toSubscribeId: number) {

    const userSubscriber = await this.userRepository.findOneBy({ id: subscriberId });

    if (userSubscriber) {
      const userFollowing = await userSubscriber.following;
      const userToSubscribe = await this.userRepository.findOneBy({ id: toSubscribeId });

      if (userToSubscribe) {
        const subscribes = userFollowing.map(User => User.id);

        if (subscribes.includes(userToSubscribe.id) === false) {

          userFollowing.push(userToSubscribe);


          await AppDataSource.manager.transaction(async transactionManager => {

            const fCounting = await AppDataSource
              .getRepository(FollowingCount)
              .createQueryBuilder()
              .where({ user: userSubscriber })
              .getOne();
            ++fCounting.followingCounter;

            await transactionManager.save(fCounting);
            await transactionManager.save(userSubscriber);

          });

          return await this.userRepository.findOne({
            where: {
              id: userSubscriber.id
            },
            relations: {
              following: true,
              followingCount: true
            }
          });
        };
        throw new HttpError(403, "Already subscribed");

      };
    };
  };

  async findUserFriends(userId) {

    const user = await this.userRepository.findOne({
      where: {
        id: userId
      }
    });

    const userFollowers = await user.followers;
    const userFollowing = await user.following;

    const leastArr = userFollowers.length < userFollowing.length ? userFollowers : userFollowing;
    const longestArr = userFollowers.length >= userFollowing.length ? userFollowers : userFollowing;

    const friendsArr = leastArr.filter(User => {
      return longestArr.some(User2 => User.id === User2.id);
    });
    return friendsArr;

  };

  async findUsersWithNoFollowing() {
    const followCounter = await this.followingCountRepository.find();
    const counterWithNoFollow = followCounter.map(fCounter => {
      if (fCounter.followingCounter == 0) {
        return fCounter;
      }
    }).filter(isNotEmpty);

    const usersWithNoFollow = await Promise.all(counterWithNoFollow.map(async fCounter => {
      return await this.followingCountRepository.findOne({
        where: {
          id: fCounter.id
        },
        relations: {
          user: true,
        },
      });
    }));

    return usersWithNoFollow;
  };

  async findTopFollowers() {

    const followCounter = await this.followingCountRepository.find();
    const counterHasFollow = followCounter.map(fCounter => {
      if (fCounter.followingCounter != 0) {
        return fCounter;
      }
    }).filter(isNotEmpty);

    const usersHasFollow = await Promise.all(counterHasFollow.map(async fCounter => {
      return await this.followingCountRepository.findOne({
        where: {
          id: fCounter.id
        },
        relations: {
          user: true,
        },
      });
    }));

    const usersSortedByFollowCounter = usersHasFollow.sort((a, b) => {

      if (a.followingCounter > b.followingCounter) {
        return -1;
      }
      if (a.followingCounter < b.followingCounter) {
        return 1;
      }
      return 0;
    });

    return usersSortedByFollowCounter;
  };

};
