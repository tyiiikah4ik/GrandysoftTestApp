import { Gender } from "../gender.enum";

export class UserDTO {

  name: string;
  gender: Gender;

}