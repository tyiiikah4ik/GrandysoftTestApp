import 'reflect-metadata';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { User } from './user.entity';


@Entity()
export class FollowingCount {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer', default: 0 })
  followingCounter!: number;

  @OneToOne(() => User, (user) => user.followingCount)
  @JoinColumn()
  user: User;

};