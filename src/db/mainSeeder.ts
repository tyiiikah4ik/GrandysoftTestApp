import "reflect-metadata";
import { DataSource } from "typeorm";
import { Seeder, SeederFactoryManager } from "typeorm-extension";
import { FollowingCount } from "../user/followingCount.entity";
import { User } from "../user/user.entity";
import * as dotenv from 'dotenv';

export default class MainSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<any> {

    dotenv.config();

    const userCount = Number(process.env.SEED_USER_COUNT);
    const subscribeCount = Number(process.env.SEED_SUBSCRIBE_COUNT);

    const userFactory = factoryManager.get(User);
    const counterFactory = factoryManager.get(FollowingCount);

    function getRandomInt(max: number) {
      return Math.floor(Math.random() * max);
    };

    const usersArr = await userFactory.saveMany(userCount)

    const userSubscribe = await Promise.all(usersArr.map(async userSubscriber => {

      const randomInt = getRandomInt(subscribeCount);
      let i = 1;

      const followingCounter = await counterFactory.make({ user: userSubscriber });

      const userFollowing = await userSubscriber.following;

      for (i; i < randomInt; i++) {
        const userToSubscribe = await usersArr.find(x => x.id == i && x.id != userSubscriber.id);
        if (userToSubscribe) {
          userFollowing.push(userToSubscribe);
          ++followingCounter.followingCounter;

        };
      };

      await dataSource.manager.transaction(async transactionManager => {
        await transactionManager.save(userSubscriber);
        await transactionManager.save(followingCounter);
      });
    })
    );

  };
};
