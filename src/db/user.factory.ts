import "reflect-metadata";
import { Faker } from '@faker-js/faker';
import { User } from "../user/user.entity";
import { setSeederFactory } from "typeorm-extension";
import { Gender } from "../user/gender.enum";

export const UserFactory = setSeederFactory(User, (faker: Faker) => {

  const user = new User();
  const genderType = faker.name.sexType();
  user.gender = <Gender>genderType;
  user.name = faker.name.firstName(genderType);

  return user;
});
