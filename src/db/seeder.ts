import "reflect-metadata";
import { DataSource, DataSourceOptions } from "typeorm";
import { runSeeders, SeederOptions } from "typeorm-extension";
import * as dotenv from "dotenv"
import { User } from "../user/user.entity";
import { FollowingCount } from "../user/followingCount.entity";
import { UserFactory } from "./user.factory";
import MainSeeder from "./mainSeeder";
import { CounterFactory } from "./counter.factory";

dotenv.config()

const options: DataSourceOptions & SeederOptions = {
  type: "postgres",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: Boolean(process.env.DB_SYNC),
  logging: false,
  entities: [User, FollowingCount],
  factories: [UserFactory, CounterFactory],
  seeds: [MainSeeder]
};

const dataSource = new DataSource(options);
dataSource.initialize().then(async () => {
  await dataSource.synchronize(true);
  await runSeeders(dataSource);
  process.exit();
});

