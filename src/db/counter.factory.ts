import "reflect-metadata";
import { Faker } from '@faker-js/faker';
import { setSeederFactory } from "typeorm-extension";
import { FollowingCount } from "../user/followingCount.entity";

export const CounterFactory = setSeederFactory(FollowingCount, (faker: Faker) => {
  const followingCounter = new FollowingCount();
  followingCounter.followingCounter = 0;

  return followingCounter;
});
