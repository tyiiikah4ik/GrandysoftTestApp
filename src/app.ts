import "reflect-metadata";
import * as express from 'express';
import * as dotenv from 'dotenv';
import { useExpressServer } from 'routing-controllers';
import { UserController } from './user/user.controller';
import { AppDataSource } from "./data-source";
import { GlobalErrorHandler } from "./middleware/globalErrorHandler";

AppDataSource.initialize()
  .then(async () => {
    console.log("Connection initialized with database...");
  })
  .catch((error) => console.log(error));

dotenv.config();

const port = process.env.SERVER_PORT;

const app = express();
app.use(express.json());

useExpressServer(app, {
  controllers: [UserController],
  middlewares: [GlobalErrorHandler],
  defaultErrorHandler: false
});



app.get('/', (req: express.Request, res: express.Response) => {
  res.send('Express + TypeScript Server');
});

app.listen(port, () => {
  console.log(`[server]: Server is running at https://localhost:${port}`);
});